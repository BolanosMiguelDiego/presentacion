@extends('plantillas.app')

@section('title', 'blog')

@section('content')

<h1>Crear post</h1>


@foreach ($errors->all() as $error)
    <p>{{ $error}}</p>
@endforeach

<form action="{{ route('post.store') }}" method="POST">
    @csrf
    <label for="">
        Title <br>
        <input name="title" type="text" value="{{ old('title') }}">
        
        @error('title')
        <br>
            <small style="color: red">{{$message}}</small>
        @enderror
    </label><br>
    <label for="">
        Body <br>
        <textarea name="body" id="" cols="30" rows="10">{{old('body')}}</textarea>
        @error('body')
        <br>
            <small style="color: red">{{$message}}</small>
        @enderror
    </label><br>
    <button type="submit">Enviar</button>
    <br>
</form>
<br>
<a href="/blog">Regresar</a>

@endsection
