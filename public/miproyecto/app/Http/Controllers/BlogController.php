<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function index(){
        $posts = [
            ['title' => 'First post'],
            ['title' => 'Second post'],
            ['title' => 'Third post'],
            ['title' => 'Fourth post']
        ];

        return view('blog', ['posts' => $posts]);
    }

    
    public function base(){

        //$posts = DB::table('posts')->get();
        $posts = Post::get();

        return view('blog', ['posts' => $posts]);
    }
/*
    public function show($post){
        return Post::find($post);
    }

    public function show(Post $post){
        return $post;
    }
    */

    public function show(Post $post){
        return view('posts.show', ['post' => $post]);
    }

    public function create(){
        return view('posts.create');
    }

    public function store(Request $request){

        $request->validate([
            'title' => ['required'],
            'body' => ['required'],
        ]);

        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->save();

        session()->flash('status', 'post created!');

        return redirect()->route('blog');
        //return to_route('posts.index');
        //return $request->input('title');
    }
    
}
