<?php

return [
    'required' => 'El campo :attribute obligatorio',
    'attributes' => [
        'title' => 'título',
        'body' => 'contenido',
    ]
];