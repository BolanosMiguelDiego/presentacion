<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* post */

/*
$posts = [
    ['title' => 'First post'],
    ['title' => 'Second post'],
    ['title' => 'Third post'],
    ['title' => 'Fourth post']
];
*/

Route::view("/login", "login")->name('login');
Route::view("/registro", "registro")->name('registro');

Route::view('/', 'welcome')->name('welcome');

Route::view('/contacto', 'contact')->name('contacto');

/* Route::view('/blog', 'blog', ['posts' => $posts])->name('blog'); */

Route::get('/blog', [BlogController::class, 'base'])->name('blog');
Route::get('/blog/create', [BlogController::class, 'create'])->name('blogC');
Route::post('/blog', [BlogController::class, 'store'])->name('post.store');
Route::get('/blog/{post}', [BlogController::class, 'show']);



Route::view('/about', 'about')->name('about');



